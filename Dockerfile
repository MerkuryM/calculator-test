FROM python:3.11-slim

WORKDIR /fromGitLab

VOLUME /allure-reports

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD python -m pytest -s -v tests-calc/* --alluredir=allure-reports/


